require "celluloid/autostart"
require 'time'

class CountdownTimer
  include Celluloid

  attr_reader :timer, :timer_str, :timer_str_full, :drop_time

  def initialize(d_time)
    @timer = 1
    @timer_str = "Countdown Ended"
    @timer_str_full = "Countdown Ended"
    @drop_time = Time.parse(d_time).getgm
  end

  def start_countdown
    while(@timer > 0)
      now = Time.now.getgm
      @timer = (@drop_time - now).to_i
      @timer_str = Time.at(@timer).utc.strftime("%H:%M:%S")
      ary = @timer_str.split(":")
      @timer_str = "#{@timer/60/60/24}D #{ary[0]}H #{ary[1]}M"
      @timer_str_full = "```\n#{@timer/60/60/24} Days #{ary[0]} Hours #{ary[1]} Minutes\n```"
      sleep(1)
    end
    @timer_str = "Countdown Ended"
    @timer_str_full = "Countdown Ended"
  end
end
